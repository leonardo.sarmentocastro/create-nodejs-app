module.exports = {
  ...require('./create-user-resolver'),
  ...require('./find-by-id-resolver'),
};
