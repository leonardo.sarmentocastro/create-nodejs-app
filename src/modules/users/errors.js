exports.userNotFoundError = () => ({
  code: 'USERS_ERROR_USER_NOT_FOUND',
  field: 'id',
});
