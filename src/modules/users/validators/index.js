module.exports = {
  ...require('./is-email-already-in-use'),
  ...require('./is-email-valid'),
  ...require('./is-required'),
  ...require('./is-username-already-in-use'),
  ...require('./is-username-too-long'),
  ...require('./validate'),
};
